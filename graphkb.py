from pyidp.typedIDP import IDP

class GraphKB(IDP):

    def __init__(self, nodes=[0], adj_list=[]):
        super(GraphKB, self).__init__()
        self.Type("Node", nodes)
        self.Predicate("Adjacent(Node,Node)", adj_list)
        self.Define("Edge(Node,Node)", 
            "lambda x,y: Adjacent(x,y) or Adjacent(y,x)")

    def add_TC(self, original, tc_name):
        formula = "lambda x,y: {0}(x,y) or any({0}(x,z) and {0}(z,y) for z in Node)".format(original)
        self.Define(tc_name + "(Node, Node)", formula)


connected = GraphKB()
cyclic = GraphKB()

connected.Define("Undirected(Node,Node)", "lambda x,y: Edge(x,y) or Edge(y,x)")
connected.Define("Path(Node,Node)", "lambda x,y: Undirected(x,y) or any(Undirected(x,z) and Path(z,y) for z in Node)")
connected.Constraint("all(Path(x,y) for x in Node for y in Node)")

cyclic.Predicate("Traverse(Node,Node)")
cyclic.Constraint("all(Edge(x,y) or Edge(y,x) for (x,y) in Traverse)")
cyclic.Constraint("not any(Traverse(y,x) for (x,y) in Traverse)")
cyclic.Define("Path(Node,Node)", "lambda x,y: Traverse(x,y) or any(Traverse(x,z) and Path(z,y) for z in Node)")
cyclic.Constraint("any(Path(x,x) for x in Node)")

def is_tree(n, adj_list):
    connected.set_nodes_edges(n, adj_list)
    cyclic.set_nodes_edges(n, adj_list)
    return not bool(cyclic.satisfiable) and bool(connected.satisfiable)

subtree = GraphKB()
subtree.Predicate("Root(Node)")
subtree.Constraint("all(any(Path(r,x) for r in Root) for x in Node if not Root(x))")
subtree.Constraint("not any(Path(x,y) for x in Root for y in Root if x != y)")
subtree.Define("Undirected(Node,Node)", "lambda x,y: Edge(x,y) or Edge(y,x)")
subtree.Define("Path(Node,Node)", "lambda x,y: Undirected(x,y) or any(Undirected(x,z) and Path(z,y) for z in Node)")
