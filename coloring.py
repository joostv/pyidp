from pyidp.typedIDP import IDP

color = IDP()
color.Type("Color", ["Blue", "Red", "Yellow", "Green"])
color.Type("Area",  ["Belgium","Holland", "Germany", "Luxembourg", "France"])
color.Predicate("Border(Area,Area)")
color.Function("Coloring(Area): Color")
color.Constraint("all(Coloring(x) != Coloring(y) for (x,y) in Border)")

#color.Area = ["Belgium","Holland", "Germany", "Luxembourg", "France"]
#color.Color= ["Blue", "Red", "Yellow", "Green"]
color.Border= [("Belgium","Holland"), ("Belgium","Germany"), 
               ("Belgium","Luxembourg"), ("Belgium","France"),
               ("Holland","Germany"), ("Germany","Luxembourg"), 
               ("Germany","France"), ("Luxembourg","France")]

for x in color.Area:
    print "Area %s has color %s" % (x, color.Coloring[x])

