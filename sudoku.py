from pyidp.typedIDP import *
import time, random

s=[ 8,5,0, 0,0,2, 4,0,0,   
    7,2,0, 0,0,0, 0,0,9,   
    0,0,4, 0,0,0, 0,0,0,
       
    0,0,0, 1,0,7, 0,0,2, 
    3,0,5, 0,0,0, 9,0,0, 
    0,4,0, 0,0,0, 0,0,0, 
    
    0,0,0, 0,8,0, 0,7,0, 
    0,1,7, 0,0,0, 0,0,0,   
    0,0,0, 0,3,6, 0,4,0 ]

idp = IDP()

d = dict(zip(range(len(s)), s))

idp.Type("Square", range(81))
idp.Predicate("SameRow(Square, Square)", [ (i, j) for i in range(81) for j in range(81) if i % 9 == j % 9])
idp.Predicate("SameCol(Square, Square)", [ (i, j) for i in range(81) for j in range(81) if i / 9 == j / 9])
idp.Predicate("SameSmallSq(Square, Square)", [ (i, j) for i in range(81) for j in range(81)
                                           if (i/9) / 3 == (j/9) / 3 and (i % 9) / 3 == (j % 9) / 3])

idp.Type("Number", range(10))
idp.Function("Given(Square): Number", d)
idp.Function("Sol(Square): Number")
idp.Define("Diff(Square, Square)", "lambda x,y: x != y and (SameRow(x,y) or SameCol(x,y) or SameSmallSq(x,y))")
idp.Constraint("all(Sol(x) == Given(x) for x in Square if Given(x) != 0)")
idp.Constraint("all(Sol(x) != 0 for x in Square)")
idp.Constraint("all(Sol(x) != Sol(y) for (x,y) in Diff)")

def show(grid):
    row = -1
    for x in idp.Square:
        import sys
        if x % 3 == 0:
            sys.stdout.write(' ')
        if x % 9 == 0:
            print ''
            row += 1
            if row % 3 == 0:
                print '' 
        sys.stdout.write(str(grid[x]) + " ")
    print ''

show(idp.Sol)


    
def solve_all(grids, name='', showif=0.0):
    """Attempt to solve a sequence of grids. Report results.
    When showif is a number of seconds, display puzzles that take longer.
    When showif is None, don't display any puzzles."""
    def time_solve(grid):
        start = time.clock()
        values = solve(grid)
        t = time.clock()-start
        show(idp.Sol)
        ## Display puzzles that take long enough
        if showif is not None and t > showif:
            display(grid_values(grid))
            if values: display(values)
            print '(%.2f seconds)\n' % t
        return (t, 0)
    times, results = zip(*[time_solve(grid) for grid in grids])
    N = len(grids)
    if N > 1:
        print "Solved %d of %d %s puzzles (avg %.2f secs (%d Hz), max %.2f secs)." % (
            sum(results), N, name, sum(times)/N, N/sum(times), max(times))

def solve(grid):
    s = grid.replace(".", "0")
    d = dict(zip(range(len(s)), s))
    idp.Given = d
    row = 0
    idp.Sol[0]
