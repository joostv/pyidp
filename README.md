# pyidp

The pyidp subdirectory contains the Python pyidp module for interfacing to the IDP system. This has been tested with IDP version 3.4, which is downloadable here:

https://dtai.cs.kuleuven.be/software/idp/

The default location of the IDP executable is specified at the top of the file "pyidp/typedIDP.py". Either update this location for your system or provide the correct location when constructing an IDP object, e.g.:

from pyidp.typedIDP import IDP
kb = IDP("location/of/IDP/bin/idp")