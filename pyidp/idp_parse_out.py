
# # -----------------------------------------------------------------------------
# # calc.py
# #
# # A simple calculator with variables -- all in one file.
# # -----------------------------------------------------------------------------

# tokens = (
#     'EQ', 'CB_OPEN', 'CB_CLOSE', 'SEMICOL', 'COMMA', 'COLON',
#     'NAME', 'QNAME', 'NUMBER', 'DOTDOT', 'ARROW'
#     )

# # Tokens

# t_EQ = r'='
# t_CB_OPEN = r'{'
# t_CB_CLOSE = r'}'
# t_SEMICOL   = r';'
# t_COMMA   = r','
# t_COLON   = r':'
# t_NAME = r'[a-zA-Z_][a-zA-Z0-9_]*'
# t_QNAME = r'"[a-zA-Z_][a-zA-Z0-9_]*"'
# t_NUMBER = r"[0-9]+"
# t_DOTDOT = r"\.\."
# t_ARROW = r'->'

# # def t_NUMBER(t):
# #     r'\d+'
# #     try:
# #         t.value = int(t.value)
# #     except ValueError:
# #         print("Integer value too large %d", t.value)
# #         t.value = 0
# #     return t

# # Ignored characters
# t_ignore = " \t\n"

# def t_error(t):
#     print("Illegal character '%s'" % t.value[0])
#     t.lexer.skip(1)
    
# # Build the lexer
# import ply.lex as lex
# lex.lex(optimize=1)

# def p_structure(t):
#     'structure : NAME COLON NAME CB_OPEN predicates CB_CLOSE'
#     t[0] = dict(t[5])

# def p_predicates(t):
#     'predicates : predicate'
#     t[0] = [t[1]]
# def p_predicates_rec(t):
#     'predicates : predicate predicates'
#     t[0] = t[2] + [t[1]]
    
# def p_predicate(t):
#     "predicate : NAME EQ CB_OPEN contents CB_CLOSE"
#     t[0] = (t[1], t[4])

# def p_pred_contents(t):
#     "contents : enumeration"
#     t[0] = set(t[1])
# def p_pred_contents_range(t):
#     "contents : NUMBER DOTDOT NUMBER"
#     t[0] = range(int(t[1]),int(t[3]))
# def p_pred_contents_func(t):
#     "contents : function_enum"
#     t[0] = dict(t[1])

# def p_enumeration(t):
#     "enumeration : tuple"
#     t[0] = [t[1]]
# def p_enumeration_rec(t):
#     "enumeration : tuple SEMICOL  enumeration"
#     t[0] = t[3] + [t[1]]

# def p_function_enum(t):
#     "function_enum : one_func_enum"
#     t[0] = [t[1]]
# def p_function_enum_rec(t):
#     "function_enum : function_enum SEMICOL one_func_enum"
#     t[0] = t[1] + [t[3]]
# def p_one_func_enum(t):
#     "one_func_enum : tuple ARROW element"
#     t[0] = (t[1], t[3])

# def p_element_qname(t):
#     "element : QNAME"
#     t[0] = t[1].strip('"')
# def p_element_number(t):
#     "element : NUMBER"
#     t[0] = int(t[1])

# def p_tuple(t):
#     "tuple : tup"
#     tup = t[1]
#     if len(tup) == 1:
#         t[0] = tup[0]
#     else:
#         t[0] = t[1]
        
# def p_tup(t):
#     "tup : element"
#     t[0] = (t[1],)
# def p_tup_rec(t):
#     "tup : element COMMA tup"
#     t[0] = (t[1],) + t[3]

# def p_error(t):
#     print("Syntax error at '%s'" % t.value)

# import ply.yacc as yacc
# yacc.yacc(start='structure')


def parse_element(s):
    s = s.strip()
    try:
        return int(s)
    except ValueError:
        unquoted = s.strip("'").strip('"')
        return unquoted

def parse_tuple(tup):
    tup = tup.strip()
    elements = tup.split(',')
    parsed = map(parse_element, elements)
    if len(parsed) == 1:
        return parsed[0]
    return tuple(parsed)

def parse_function_tuple(s):
    s = s.strip()
    from_, to = s.split('->')
    return parse_tuple(from_), parse_element(to)
    
def parse_enumerated(s):
    if '->' in s:
        return parse_function_tuple(s)
    return parse_tuple(s)

def parse_enumeration(s):
    s = s.strip()
    elements = s.split(';')
    parsed = map(parse_enumerated, elements)
    if "->" in s: # Function
        return dict(parsed)
    else: # Predicate
        return parsed

def parse_range(s):
    s = s.strip()
    low, up = s.split('..')
    return range(int(low),int(up))

def parse_contents(s):
    stripped = s.strip().lstrip('{').rstrip('}').strip()
    if '..' in stripped:
        return parse_range(stripped)
    return parse_enumeration(stripped)

def parse_assignment(s,wanted):
    name, contents = s.split('=')
    name = name.strip()
    if name in wanted:
        return name, parse_contents(contents)
    else:
        return None, None

def idp_parse(s,wanted):
    lines = s.splitlines()
    first_line = True
    res = {}
    for line in lines:
        line = line.strip()
        if first_line:
            first_line = False
            continue
        if line == "" or line.rstrip('}') == "":
            continue
        symb, content = parse_assignment(line,wanted)
        if symb:
            res[symb] = content
    return res
        
