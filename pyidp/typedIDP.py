IDP_LOCATION = "/Users/joost/idp-3.4.0-darwin/bin/idp"

from idp_py_syntax import parse_formula

from idpobjects import *

class Block(object):

    def __init__(self, name):
        self.name = name

    def show(self, objects):
        return self.header() + self.begin() + self.content(objects) + self.end()

    def begin(self):
        return " {\n"

    def method(self):
        return "in_" + self.__class__.__name__.lower()

    def content(self, objects):
        def types_first(x):
            return (1 if isinstance(x, IDPType) else 2)
        def content_for(x):
            m = getattr(x, self.method())
            return m()
        return "\n".join(map(content_for, sorted(objects, key=types_first)))

    def end(self):
        return "\n} \n"

    def header(self):
        raise NotImplementedError("Abstract method")

class Theory(Block):

    def __init__(self, name, voc):
        super(Theory,self).__init__(name)
        self.voc = voc
        
    def header(self):
        return "theory " + self.name + " : " + self.voc

class Structure(Block):

    def __init__(self, name, voc):
        super(Structure,self).__init__(name)
        self.voc = voc

    def header(self):
        return "structure " + self.name + " : " + self.voc

class Vocabulary(Block):

    def header(self):
        return "vocabulary " + self.name

def subclasses(cls):
    return reduce(lambda x,y: x + y, map(lambda x: subclasses(x) + [x], cls.__subclasses__()), [])

class IDP(object):

    def __init__(self, executable=IDP_LOCATION):
        self.__dict__['executable'] = executable
        self.__dict__['idpobjects'] = {}
        self.__dict__['wanted'] = []
        self.__dict__['object_names'] = {}
        self.__dict__['cache'] = None
        self.__dict__['dirty'] = True
        self.Predicate("satisfiable()")

    @staticmethod
    def split_pred_name(pred_name):
        pred_name = pred_name.strip()
        if not pred_name.endswith(")"):
            pred_name += "()"
        name, args = pred_name.split("(")
        arglist = args.strip(')').split(",")
        return name, arglist

    @staticmethod
    def split_func_name(func_name):
        func, return_type = func_name.split(":")
        name, arglist = IDP.split_pred_name(func)
        return name, arglist, return_type.strip()

    def Predicate(self, typed_name, enumeration=None):
        name, arglist = self.split_pred_name(typed_name)
        if enumeration != None:
            res = IDPEnumeratedPredicate(self, name, arglist, enumeration)
        else:
            res = IDPUnknownPredicate(self, name, arglist)
        self.know(res)
    
    def Type(self, name, enumeration):
        if len(enumeration) > 0 and all([isinstance(x, (int,long)) for x in enumeration]):
            res = IDPIntType(self, name, enumeration)
        else:
            res = IDPType(self, name, enumeration)
        return self.know(res)

    def Constraint(self,formula,already_IDP = False):
        idp_formula = formula if already_IDP else parse_formula(formula)
        res = IDPConstraint(self, idp_formula)
        self.append(res)

    def Function(self, typed_name, enumeration = None, partial=False):
        func, arg_list, return_type = self.split_func_name(typed_name)
        if enumeration is not None:
            res = IDPEnumeratedFunction(self, func, arg_list, return_type, enumeration, partial=partial)
        else:
            res = IDPUnknownFunction(self, func, arg_list, return_type, partial=partial)
        return self.know(res)

    def Constant(self, typed_name, enumeration = None):
        if enumeration != None:
            enum2 = { (): enumarion }
        return self.Function(typed_name, enum2)
    
    def GeneratedFunction(self, typed_name, impl):
        func, arg_list, return_type = self.split_func_name(typed_name)
        res = IDPGeneratedFunction(self, func, arg_list, return_type, impl)
        return self.know(res)

    def GeneratedPartialFunction(self, typed_name, impl):
        func, arg_list, return_type = self.split_func_name(typed_name)
        res = IDPGeneratedPartialFunction(self, func, arg_list, return_type, impl)
        return self.know(res)

    def Define(self, *args):
        already_idp = isinstance(args[-1],bool) and args.pop()
        if len(args) == 2: # Called as "Define(Head,Body)"
           args = [tuple(args)]
        elif len(args) == 1: # Called as "Define([(H1,B1)...])"
            args = args[0]
        rules = []
        for (head, body) in args:
            if not head.endswith(")"):
                head = head = "()"
            idp_form = body if already_idp else parse_formula(body)
            pred, types = head.split("(")
            types = types.rstrip(")").split(",")
            self.Predicate(pred + "(" + ','.join(types) + ")")
            idp_form = parse_formula(body)
            rules.append(IDPRuleStr(self, pred + idp_form))
        self.append(IDPDefinition(self, rules))

    # Format the contents of one of our IDPObjects to a str for IDP
    def for_idp(self, thing):
        if isinstance(thing,str):
            return thing
        return str(thing)
    
    def assign_name(self, object_):
        if isinstance(object_, (int,long,str,bool,float)): # Primitive type
            return object_
        name = "o" + str(id(object_))
        self.object_names[name] = object_
        return name

    # Normal forms for everything that comes from the Python world to an IDPObject
    def normal_form(self, tup):
        if isinstance(tup, tuple):
            if len(tup) == 1:
                return self.normal_form(tup[0])
            else:
                return tuple(map(self.normal_form, tup))
        return self.assign_name(tup)

    def normal_form_enumeration(self, enum):
        if isinstance(enum, dict):
            return { self.normal_form(x) : self.normal_form(enum[x]) for x in enum }
        else:
            return [ self.normal_form(x) for x in enum ]

    def object_for_name(self, name):
        return self.object_names[name]

    def append(self,p):
        if isinstance(p, IDPUnknownObject):
            self.wanted.append(p)
        try: 
            name = p.name
        except AttributeError:
            name = id(p)
        self.idpobjects[name] = p
        self.dirty = True
        
    def know(self, p):
        self.append(p)
        return p

    def forget(self, old):
        del self.idpobjects[old.name]
        if old in self.wanted:
            self.wanted.remove(old)

    def renew(self, old, new):
        old_class = old.__class__
        old_tn = old.typedName()
        self.know(old_class(old.typedName, new))

    def __str__(self):
        return "\n".join(map(lambda bl: bl.show(self.idpobjects.values()), self.blocks)) + "\n" + IDP.gen_models + "\n"

    def fillIn(self, pred):
        if self.dirty:
            self.refresh()
        return self.cache[pred]

    def __setattr__(self, name, val):
        if name not in self.idpobjects:
            if name in self.__dict__:
                self.__dict__[name] = val
                return # Internal workings 
    #   if name not in self.idpobjects:
            else:
                raise ValueError("Please declare " + name + " before assigning to it.")
        old = getattr(self, name)
        old_class = old.__class__
        old_typ_name = old.typedName()
        self.forget(old)
        if issubclass(old_class, IDPType):
            return self.Type(old_typ_name, val)
        if issubclass(old_class, IDPPredicate):
            return self.Predicate(old_typ_name, val)
        if issubclass(old_class, IDPFunction):
            return self.Function(old_typ_name, val)
        raise ValueError("Failed to update " + str(old))

    def __getattr__(self, name):
        name = name.strip()
        return self.idpobjects[name]

    def refresh(self):
        from subprocess import Popen, PIPE, STDOUT
        import os
        if __debug__:
            print "SENT TO IDP:"
            print self
            print "END OF IDP INPUT"
        devnull = open(os.devnull, 'wb')
        idp = Popen(self.executable, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        out, err = idp.communicate(input=str(self))
        if err and __debug__:
            print "IDP Error Message:"
            print err
        if __debug__:
            print "GOT OUTPUT:"
            print out
            print "END OF IDP OUTPUT"        
        import idp_parse_out
        if out.strip() == "nil":
            print "UNSATISFIABLE!"
            self.cache = {'satisfiable' : []}
        else:
#            self.Predicate("satisfiable", [()])
            self.cache = idp_parse_out.idp_parse(out, map(lambda x: x.name, self.wanted))
        self.dirty = False
    
    gen_models = """procedure main(){
    stdoptions.xsb = true
	stdoptions.nbmodels = 1    //tell IDP to search for all models (in the modelexpand inference)
	allsols = modelexpand(T,S)
	print(allsols[1])
        }"""

    fill_in_pred = """procedure main(){
	stdoptions.nbmodels = 1
	allsols = modelexpand(T,S)
	if #allsols ~= 1 then
		print("something is terribly wrong")
	else
		print(allsols[1])
	end
        }"""

    blocks = [Vocabulary("V"), Theory("T", "V"), Structure("S","V")]

    def Property(self,typing):
        f = self.Function(typing)
        return property(lambda x: f[x])
    
    def PartialProperty(self,typing):
        f = self.Function(typing,partial=True)
        def upd(self,tar):
            f[self] = tar
        return property(lambda x: f[x], upd)
    
    def KnownProperty(self,typing):
        f = self.Function(typing,dict(),partial=True)
        def upd(self,tar):
            f[self] = tar
        return property(lambda x: f[x], upd)
    
def type(idp):
    def foo(cls):
        clsname = cls.__name__
        for name, method in cls.__dict__.iteritems():
            if hasattr(method, "_idp_return_type"):
                rt = getattr(method, "_idp_return_type")
                if isinstance(rt, str):
                    rt_name = rt
                else:
                    rt_name = rt.__name__
                typed_name = name + "(" + clsname + ")" + " : " + rt_name
                if hasattr(method, "_partial"):
                    idp.GeneratedPartialFunction(typed_name, method)
                else:
                    idp.GeneratedFunction(typed_name, method)
        class Sub(cls):
            def __init__(self, *args, **kw):
                super(Sub,self).__init__(*args, **kw)
                type_ = getattr(idp, clsname)
                type_.add(idp.assign_name(self))
        Sub.__name__ = clsname +"!"
        Sub.__module__ = cls.__module__
        idp.Type(cls.__name__,set([]))
        return Sub        
    return foo

def function_to(target):
    def wrapper(func):
        func._idp_return_type = target
        return func
    return wrapper

def partial_function_to(target):
    def wrapper(func):
        func._idp_return_type = target        
        func._partial = True
        return func
    return wrapper

def idp_property(typing):
    return property(lambda x: x)
